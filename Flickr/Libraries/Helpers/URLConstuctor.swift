//
//  URLConstuctor.swift
//  Flickr
//
//  Created by Александр on 22.04.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation

func costructURL(farm: Int, server: String, id: String, secret: String) -> URL? {
    let request = "https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret)_z.jpg"
    return URL(string: request)
}
