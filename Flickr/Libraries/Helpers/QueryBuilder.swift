//
//  QueryBuilder.swift
//  Flickr
//
//  Created by Александр on 22.04.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import FlickrKit

class QueryBuilder: NSObject {

    private var query = FKFlickrPhotosSearch()

    // MARK: - Building

    func perPage(number: String) -> QueryBuilder {
        query.per_page = number
        return self
    }

    func forPage(page: Int) -> QueryBuilder {
        query.page = "\(page)"
        return self
    }

    func search(text: String) -> QueryBuilder {
        query.text = text
        return self
    }

    func searchLocation(center: coord) -> QueryBuilder {
        query.lat = "\(center.latitude)"
        query.lon = "\(center.longtitude)"
        return self
    }

    func searchCircle(radius: Int) -> QueryBuilder {
        query.radius = "\(radius)"
        return self
    }

    func build() -> FKFlickrPhotosSearch {
        return self.query
    }
}
