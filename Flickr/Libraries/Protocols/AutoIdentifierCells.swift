//
//  AutoIdentifierCells.swift
//  Flickr
//
//  Created by Александр on 23.04.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation

import UIKit

protocol AutoIndentifierCell: class {
    static var identifier: String { get }

    static var nibName: String { get }
}

extension AutoIndentifierCell where Self: UICollectionReusableView {
    static var identifier: String {
        return self.nibName
    }

    static var nibName: String {
        return String(describing: self)
    }
}
