//
//  ScrollViewCalculations.swift
//  Flickr
//
//  Created by Александр on 23.04.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation
import UIKit

extension UIScrollView {
    func calculatePullHeight() -> CGFloat {
        let contentOffset = self.contentOffset.y
        let contentHeight = self.contentSize.height
        let diffHeight    = contentHeight - contentOffset
        let frameHeight   = self.bounds.size.height
        let pullHeight    = diffHeight - frameHeight
        return pullHeight
    }
}
