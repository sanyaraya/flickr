//
//  AlertUtility.swift
//  Together
//
//  Created by Александр on 11.10.2018.
//  Copyright © 2018 com.Think.Dev.Together. All rights reserved.
//

import UIKit

extension UIViewController {

    func showAlertView(title: String, message: String, viewController: UIViewController) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            viewController.present(alert, animated: true, completion: nil)
        }
    }

    public func showErrorAlert(error: Error?) {
        showAlertView(title: ErrorTitles.Title, message: error?.localizedDescription ?? ErrorTitles.Default, viewController: self)
    }

    public func showErrorAlert(error: String) {
        showAlertView(title: ErrorTitles.Title, message: error, viewController: self)
    }
}
