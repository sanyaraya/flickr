//
//  ImageLoader.swift
//  Flickr
//
//  Created by Александр on 27.04.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit

class ImageLoader: NSObject {
    // MARK: - Properties

    var task: URLSessionDataTask?
    
    // MARK: - loading

    func loadImage(model: PhotoModel, completion:@escaping((UIImage?, String) -> Void)) {
        self.cancelLoading()

        guard let url = model.photoUrl else {
            completion(nil, model.id)
            return
        }

        self.task = URLSession.shared.dataTask(with: url, completionHandler: {[weak self] (data, response, error) in
            defer {self?.task = nil}

            guard self?.task?.state != .canceling else {
                return
            }

            guard let imageData = data else {
                return
            }
            
            let image = UIImage(data: imageData)
           
            completion(image, model.id)
        })
        self.task?.resume()
    }

    func cancelLoading() {
        self.task?.cancel()
        self.task = nil
    }
}
