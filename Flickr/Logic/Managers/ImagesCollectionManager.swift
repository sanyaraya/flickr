//
//  ImagesCollectionManager.swift
//  Flickr
//
//  Created by Александр on 02.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit

//MARK: - ImagesCollectionManagerDelegate

protocol ImagesCollectionManagerDelegate: class {

    func manager(_ manager: ImagesCollectionManager, numberOfItemsInSection section: Int) -> Int

    func manager(_ manager: ImagesCollectionManager, modelForItemAt indexPath: IndexPath) -> PhotoModel?
    
    func manager(loadMoreItems manager: ImagesCollectionManager)
    
    func manager(_ manager: ImagesCollectionManager, showInDetails model: PhotoModel)
}

extension ImagesCollectionManagerDelegate {
    func manager(loadMoreItems manager: ImagesCollectionManager) {}
}

//MARK: - ImagesCollectionManager

class ImagesCollectionManager: NSObject {
    
    //MARK: - Constants
    
    private enum Defaults {
        static let pullEdge: CGFloat = 100.0
        static let footerHeight: CGFloat = 55.0
        static let downPosition: CGFloat = 0.0
    }
    
    //MARK: - Properties
    
    let collectionView: UICollectionView
    
    var footerView: LoadableFooterView?
    
    weak var delegate: ImagesCollectionManagerDelegate?
    
    init(collection: UICollectionView) {
        self.collectionView = collection
        super.init()
        self.registerCell()
        self.configureFooter()
        self.collectionView.delegate   = self
        self.collectionView.dataSource = self
    }
    
    // MARK: - Preparation Methods
    
    public func registerCell() {
        self.collectionView.register(UINib(nibName: PhotoCell.nibName, bundle: nil), forCellWithReuseIdentifier: PhotoCell.identifier)
        self.collectionView.register(UINib(nibName: LoadableFooterView.nibName, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: LoadableFooterView.identifier)
    }
    
    private func configureFooter() {
        guard let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        layout.footerReferenceSize = CGSize(width: self.collectionView.bounds.size.width, height: Defaults.footerHeight)
    }
}

// MARK: - UICollectionViewDataSource

extension ImagesCollectionManager: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.delegate?.manager(self, numberOfItemsInSection: section) ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView
            .dequeueReusableCell(withReuseIdentifier: PhotoCell.identifier, for: indexPath) as! PhotoCell
        if let model = self.delegate?.manager(self, modelForItemAt: indexPath) {
            cell.model = model
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionFooter {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: LoadableFooterView.identifier, for: indexPath) as! LoadableFooterView
            self.footerView = footerView
            self.footerView?.backgroundColor = UIColor.clear
            
            return footerView
        } else {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: LoadableFooterView.identifier, for: indexPath)
            return headerView
        }
    }
}

// MARK: - UICollectionViewDelegate

extension ImagesCollectionManager: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
       
        if let model = self.delegate?.manager(self, modelForItemAt: indexPath) {
            self.delegate?.manager(self, showInDetails: model)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        if elementKind == UICollectionView.elementKindSectionFooter {
            DispatchQueue.main.async {
                self.footerView?.isHidden = true
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width, height: Defaults.footerHeight)
    }
    
    // MARK: - UIScrollViewDelegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard let footer = self.footerView else {
            return
        }
        
        let pullHeight = abs(scrollView.calculatePullHeight())
        
        guard pullHeight == Defaults.downPosition else {
            return
        }
        
        DispatchQueue.main.async {
            footer.isHidden = false
        }
        self.delegate?.manager(loadMoreItems: self)
    }
}

