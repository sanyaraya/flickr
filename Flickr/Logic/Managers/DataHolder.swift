//
//  BaseHolder.swift
//  Flickr
//
//  Created by Александр on 23.04.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit

class DataHolder {

    private var photos = [PhotoModel]()
    
    // MARK: - Getters

    var setsNumber = 0
    
    var itemsNumber: Int {
        return self.photos.count
    }

    subscript(index: Int) -> PhotoModel? {
        return getPhoto(index: index)
    }
    
    func getPhoto(index: Int) -> PhotoModel? {
        if index < 0 || index >= self.photos.count {
            return nil
        }
        return self.photos[index]
    }

    // MARK: - loading query

    func loadMore(query: String, completion: @escaping((_ success: Bool, _ error: Error?) -> Void)) {
        DataManager.shared.getPhotos(page: self.setsNumber + 1, query: query) {[unowned self] (set, error) in
            if error != nil {
                completion(false, error)
                return
            }
            self.appendSet(set: set, completion: completion)
        }
    }

    func reload(query: String, completion: @escaping((_ success: Bool, _ error: Error?) -> Void)) {
        let photos = self.photos
        self.removePhotos()
        self.loadMore(query: query) {[unowned self] (success, error) in
            if !success {
                self.photos = photos
            }
            completion(success, error)
        }
    }

    // MARK: - loading location

    func loadMore(location: coord, completion: @escaping((_ success: Bool, _ error: Error?) -> Void)) {
     
        DataManager.shared.getPhotos(page: self.setsNumber + 1, location: location) { (set, error) in
            if error != nil {
                completion(false, error)
                return
            }
            self.appendSet(set: set, completion: completion)
        }
    }

    func reload(location: coord, completion: @escaping((_ success: Bool, _ error: Error?) -> Void)) {
        let photos = self.photos
        self.removePhotos()
        self.loadMore(location: location) {[unowned self] (success, error) in
            if !success {
                self.photos = photos
                self.setsNumber = 1
            }
            completion(success, error)
        }
    }

    // MARK: - Helpers
    
    func removePhotos() {
        self.photos.removeAll()
        self.setsNumber = 0
    }

    func appendSet(set: PhotoSet?, completion: (@escaping(Bool, Error?) -> Void)) {
        guard set != nil else {
            let code = ErrorCodes.Unknown
            completion(false, NSError(domain: "", code: code.rawValue, userInfo: code.getUserInfo()))
            return
        }
        
        if set?.page != self.setsNumber {
            self.photos += set?.photo ?? []
            self.setsNumber += 1
        }
        completion(true, nil)
    }
}
