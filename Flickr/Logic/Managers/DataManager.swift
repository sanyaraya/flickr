//
//  DataManager.swift
//  Flickr
//
//  Created by Александр on 22.04.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import FlickrKit
import MoreCodable

class DataManager: NSObject {

    // MARK: - Constants

    private enum Keys {
        static let photosKey = "photos"
    }

    private enum Defaults {
        static let NumberPerPage = "36"
        static let SearchedRadius = 1
    }

    static let shared = DataManager()

    // MARK: - Photos Loading

    func loadPhotos(page: Int, query: String, completion: @escaping((Any?, Error?) -> Void)) {
        let query = QueryBuilder().forPage(page: page).perPage(number: Defaults.NumberPerPage).search(text: query).build()

        FlickrKit.shared().call(query, completion: completion)
    }

    func loadPhotos(page: Int, center: coord, completion: @escaping((Any?, Error?) -> Void)) {
        let query = QueryBuilder().forPage(page: page).perPage(number: Defaults.NumberPerPage).searchLocation(center: center).searchCircle(radius: Defaults.SearchedRadius).build()

        FlickrKit.shared().call(query, completion: completion)
    }

    // MARK: - Photos Parsing

    func getPhotos(page: Int, query: String, completion: @escaping((PhotoSet?, Error?) -> Void)) {
        self.loadPhotos(page: page, query: query) {[weak self] (response, error) in
            self?.parseResponse(response: response, error: error, completion: completion)
        }
    }

    func getPhotos(page: Int, location: coord, completion: @escaping((PhotoSet?, Error?) -> Void)) {
        self.loadPhotos(page: page, center: location) {[weak self] (response, error) in
            self?.parseResponse(response: response, error: error, completion: completion)
        }
    }

    // MARK: - Helper

    func parseResponse(response: Any?, error: Error?, completion: @escaping((PhotoSet?, Error?) -> Void)) {
        guard error == nil else {
            completion(nil, error)
            return
        }
   
        guard let res = response as? [String: Any?], let object = res[Keys.photosKey] as? [String: Any?] else {
            let code = ErrorCodes.Unknown
            completion(nil, NSError(domain: "", code: code.rawValue, userInfo: code.getUserInfo()))
            return
        }
        
        let parsedResult: PhotoSet? = try? DictionaryDecoder().decode(PhotoSet.self, from: object)
        completion(parsedResult, nil)
    }
}
