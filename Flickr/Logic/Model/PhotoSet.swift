//
//  Photos.swift
//  Flickr
//
//  Created by Александр on 22.04.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation

struct PhotoSet: Decodable {
    let page: Int
    let pages: Int
    let photo: [PhotoModel]
}
