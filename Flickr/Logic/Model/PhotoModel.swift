//
//  PhotoModel.swift
//  Flickr
//
//  Created by Александр on 22.04.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation

struct PhotoModel: Decodable {
    let id: String
    let title: String
    let photoUrl: URL?

    private enum CodingKeys: String, CodingKey {
        case id
        case farm
        case title
        case secret
        case server
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.title = try container.decode(String.self, forKey: .title)
        self.id    = try container.decode(String.self, forKey: .id)
        
        let secret = try container.decode(String.self, forKey: .secret)
        let server = try container.decode(String.self, forKey: .server)
        let farm   = try container.decode(Int.self, forKey: .farm)
        
        self.photoUrl = costructURL(farm: farm, server: server, id: self.id, secret: secret)
    }
}
