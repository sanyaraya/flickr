//
//  PhotosViewContollerViewController.swift
//  Flickr
//
//  Created by Александр on 22.04.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit

class PhotosBaseController: UIViewController {

    // MARK: - Constants

    private enum Defaults {
        static let columnsNumber = 3
    }

    // MARK: - Properties

    @IBOutlet weak var cvPhotos: InstStyledCollectionView!

    var footerView: LoadableFooterView?
    let data = DataHolder()
    var collection: ImagesCollectionManager?
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.cvPhotos.columnsNumber = Defaults.columnsNumber
        self.collection = ImagesCollectionManager(collection: self.cvPhotos)
        self.collection?.delegate = self
    }

    func manager(loadMoreItems manager: ImagesCollectionManager) {}
    
    //MARK: - Helper
    
    func photosDidLoad(success: Bool, error: Error?) {
        if success {
            DispatchQueue.main.async {
                self.cvPhotos.reloadData()
            }
        } else {
            self.showErrorAlert(error: error)
        }
    }
}

//MARK: - ImagesCollectionManagerDelegate

extension PhotosBaseController: ImagesCollectionManagerDelegate {
  
    func manager(_ manager: ImagesCollectionManager, numberOfItemsInSection section: Int) -> Int {
        return self.data.itemsNumber
    }
    
    func manager(_ manager: ImagesCollectionManager, modelForItemAt indexPath: IndexPath) -> PhotoModel? {
        return self.data[indexPath.row]
    }
    
    func manager(_ manager: ImagesCollectionManager, showInDetails model: PhotoModel) {
        let detailVC = DetailPhotoViewController.getInstance(model: model)
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
}


