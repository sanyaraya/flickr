//
//  PhotoCell.swift
//  Flickr
//
//  Created by Александр on 22.04.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {

    // MARK: - Properties

    @IBOutlet weak var ivPhoto: UIImageView!
    @IBOutlet weak var aiLoad: UIActivityIndicatorView!

    let loader = ImageLoader()
    
    var model: PhotoModel? {
        didSet {
            self.loadImage()
        }
    }

    // MARK: - Lifecycle
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.loader.cancelLoading()
    }

    // MARK: - Methods

    private func loadImage() {
        guard let newModel = model else {
            return
        }
        
        self.aiLoad.startAnimating()
        self.loader.loadImage(model: newModel) { [weak self] (image, id) in
            DispatchQueue.main.async {
                self?.aiLoad.stopAnimating()
                guard id == self?.model?.id else {
                    return
                }
                self?.ivPhoto.image = image ?? UIImage(named: Images.DefaultImage)
            }
        }
    }
}

// MARK: - AutoIndentifierCell

extension PhotoCell: AutoIndentifierCell {}
