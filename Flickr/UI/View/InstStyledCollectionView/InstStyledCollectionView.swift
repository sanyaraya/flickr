//
//  InstStyledCollectionView.swift
//  Flickr
//
//  Created by Александр on 23.04.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit

class InstStyledCollectionView: UICollectionView {

    // MARK: - Properties

    var columnsNumber = 3 {
        didSet {
            self.setColumns(number: columnsNumber, spacing: cellSpacing, cellRatio: cellRatio)
        }
    }

    var cellSpacing = 2 {
        didSet {
            self.setColumns(number: columnsNumber, spacing: cellSpacing, cellRatio: cellRatio)
        }
    }

    var cellRatio = 1.0 {
        didSet {
           self.setColumns(number: columnsNumber, spacing: cellSpacing, cellRatio: cellRatio)
        }
    }

    // MARK: - Methods

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setColumns(number: columnsNumber, spacing: cellSpacing, cellRatio: cellRatio)
    }

    func setColumns(number: Int, spacing: Int, cellRatio: Double) {
       self.layoutIfNeeded()
        guard let flowLayout = self.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }

        flowLayout.minimumLineSpacing = CGFloat(spacing)
        flowLayout.minimumInteritemSpacing = CGFloat(spacing)

        let spacing   = flowLayout.minimumInteritemSpacing
        let itemWidth = (self.frame.size.width - CGFloat(number - 1) * spacing) / CGFloat(number)
        flowLayout.itemSize =  CGSize(width: itemWidth, height: itemWidth * CGFloat(cellRatio))
    }
}
