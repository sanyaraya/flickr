import Foundation
import UIKit

class LoadableFooterView: UICollectionReusableView {}

extension LoadableFooterView: AutoIndentifierCell {}
