//
//  LocationsPhotoController.swift
//  Flickr
//
//  Created by Александр on 23.04.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import CoreLocation

class LocationsPhotoController: PhotosBaseController {

    var locationManager = LocationManager()

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager.delegate = self
        self.locationManager.searchLocation()
    }
    
    override func manager(loadMoreItems manager: ImagesCollectionManager) {
        guard let location = self.locationManager.location else {
            showErrorAlert(error: ErrorCodes.description(code: ErrorCodes.LocationFailed.rawValue))
            return
        }
        self.data.loadMore(location: location) {[weak self] (success, error) in
            self?.photosDidLoad(success: success, error: error)
        }
    }
}

// MARK: - LocationManagerDelegate

extension LocationsPhotoController: LocationManagerDelegate {
    func showError(description: String) {
        showErrorAlert(error: description)
    }

    func locationDidUpdated(location: coord) {
        self.data.reload(location: location) {[weak self] (success, _) in
            if success {
                DispatchQueue.main.async {
                    self?.cvPhotos.reloadData()
                }
            }
        }
    }
}

