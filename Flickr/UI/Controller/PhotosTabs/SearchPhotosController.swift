//
//  SearchPhotosController.swift
//  Flickr
//
//  Created by Александр on 23.04.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit

class SearchPhotosController: PhotosBaseController {

    // MARK: - Properties

    private enum Defaults {
        static let requestDelay = 400
    }

    @IBOutlet weak var sbSearch: UISearchBar!

    private var pendingRequestWorkItem: DispatchWorkItem?

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadDefaultsIfNeeded()
    }
    
    override func manager(loadMoreItems manager: ImagesCollectionManager) {
        guard let query = self.sbSearch.text else {
            showErrorAlert(error: ErrorCodes.description(code: ErrorCodes.InvalidInput.rawValue))
            return
        }
        
        self.data.loadMore(query: query) { [weak self](success, error) in
            self?.photosDidLoad(success: success, error: error)
        }
    }
    
    //MARK: - Functions

    private func loadDefaultsIfNeeded() {
        if let query = UserDefaults.standard.string(forKey: DefaultsKeys.kLastSearchQuery) {
            self.sbSearch.text = query
            self.searchBar(self.sbSearch, textDidChange: query)
        }
    }
}

// MARK: - UISearchBarDelegate

extension SearchPhotosController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.pendingRequestWorkItem?.cancel()

        guard searchText.count != 0 else {
            return
        }

        let requestWorkItem = DispatchWorkItem { [weak self] in
            self?.data.reload(query: searchText, completion: { (success, error) in
                if success {
                    UserDefaults.standard.set(searchText, forKey: DefaultsKeys.kLastSearchQuery)
                    DispatchQueue.main.async {
                        self?.cvPhotos.reloadData()
                    }
                } else {
                   self?.showErrorAlert(error: error)
                }
            })
        }

        self.pendingRequestWorkItem = requestWorkItem
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(Defaults.requestDelay),
                                      execute: requestWorkItem)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

