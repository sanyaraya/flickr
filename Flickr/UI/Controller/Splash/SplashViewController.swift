//
//  SplashViewController.swift
//  Flickr
//
//  Created by Александр on 23.04.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    @IBOutlet weak var ivLogo: UIImageView!

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = true

        self.rotateImage()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            self.tabBarController?.tabBar.isHidden = false
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.popViewController(animated: false)
        }
    }

    // MARK: - Animation

    func rotateImage() {
        UIView.animate(withDuration: 1.0, delay: 0, options: [.repeat, .autoreverse], animations: {
            self.ivLogo.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        }, completion: nil)
    }
}

// MARK: - StoryboardInstantiable

extension SplashViewController: StoryboardInstantiable {
    static var storyboardName: String {
        return Storyboards.Splash
    }
}
