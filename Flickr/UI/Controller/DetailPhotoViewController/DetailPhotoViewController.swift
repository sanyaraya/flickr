//
//  PhotoViewController.swift
//  Flickr
//
//  Created by Александр on 23.04.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit

class DetailPhotoViewController: UIViewController {

    // MARK: - Constants

    private enum Defaults {
        static let maxZoomScale: CGFloat = 6.0
        static let minZoomScale: CGFloat = 1.0
    }

    // MARK: - Properties

    var model: PhotoModel!
    let loader = ImageLoader()

    @IBOutlet weak var vFooter: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var ivPhoto: UIImageView!

    @IBOutlet weak var svContent: UIScrollView!

    @IBOutlet weak var aiView: UIActivityIndicatorView!

    // MARK: - Lifecycle

    static func getInstance(model: PhotoModel) -> DetailPhotoViewController {
        let vc = DetailPhotoViewController.storyboardInstance()!
        vc.model = model
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()        
        self.loadDetails()
        self.configureScrollView()
    }

    // MARK: - Functions

    private func loadDetails() {
        self.lblTitle.text = "\(model.title)"
        self.vFooter.isHidden = (self.lblTitle.text?.count == 0)
        self.loader.loadImage(model: self.model!) { (img, _) in
            DispatchQueue.main.async {
                self.ivPhoto.image = img ?? UIImage(named: Images.DefaultImage)
                self.aiView.stopAnimating()
            }
        }
    }

    private func configureScrollView() {
        self.svContent.maximumZoomScale = Defaults.maxZoomScale
        self.svContent.minimumZoomScale = Defaults.minZoomScale
    }
}

// MARK: - UIScrollViewDelegate

extension DetailPhotoViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.ivPhoto
    }
}

// MARK: - StoryboardInstantiable

extension DetailPhotoViewController: StoryboardInstantiable {
    static var storyboardName: String {
        return Storyboards.Photo
    }
}
