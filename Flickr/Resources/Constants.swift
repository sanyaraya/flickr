//
//  Constants.swift
//  Flickr
//
//  Created by Александр on 22.04.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation

// MARK: - API

enum API {
    static let APIKey = "68f0f1267eab919d337916ca2fb4dd4b"
    static let Secret = "349c0ea5a4d43989"
}

// MARK: - Errors

enum ErrorTitles {
    static let Title              = "Ooooopss"
    static let Default            = "Something went wrong"
    static let EnableUserLocation = "We need your location. Please, enable finding user locations"
}

enum ErrorCodes: Int {
    case Unknown   = 5000
    case LastPage
    case InvalidInput
    case LocationFailed

    static func description(code: Int) -> String {
        let state = ErrorCodes(rawValue: code)
        switch state {
            case .Unknown?:
                return "Unknown error"
            case .LastPage?:
                return "Last page reached"
            case .InvalidInput?:
                return "Input is invalid. Please, check"
            case .LocationFailed?:
                return "Location loading failed"
            default:
                return "Unknown error"
            }
    }

    func getUserInfo() -> [String: Any] {
        let info: [String: Any] = [
            NSLocalizedDescriptionKey: (ErrorCodes.description(code: self.rawValue) as Any)]
        return info
    }
}

// MARK: - Storyboards

enum Storyboards {
    static let Photo  = "Photo"
    static let Splash = "Splash"
    static let Main   = "Main"
}

// MARK: - UserDefaults

enum DefaultsKeys {
    static let kLastSearchQuery  = "LastSearchQuery"
}

// MARK: - Images

enum Images {
    static let DefaultImage = "app_default_image.png"
}

// MARK: - Typealias

typealias coord = (latitude: Double, longtitude: Double)
